package com.chopikus.bizzarepizzadrivers;

public class OrderModel {

    String name="", addressFrom="", addressTo="", status="";
    String id="";
    String number="";
    public OrderModel(String name, String addressFrom, String addressTo, String id, String number) {
        this.name = name;
        this.addressTo = addressTo;
        this.addressFrom = addressFrom;
        this.id = id;
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public String getAddressFrom() {
        return addressFrom;
    }

    public String getAddressTo() {
        return addressTo;
    }

    public String getStatus() {
        return status;
    }

    public String getId() {
        return id;
    }
}