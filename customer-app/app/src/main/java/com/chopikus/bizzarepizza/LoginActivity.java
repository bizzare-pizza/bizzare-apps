package com.chopikus.bizzarepizza;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class LoginActivity extends AppCompatActivity {
    private String phoneNumber = "";
    //sync methods are using UI, async not
    int currentLayout=-1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkForNumber();
    }
    void setScreen(int id)
    {
        if (currentLayout!=id) {
            setContentView(id);
            currentLayout = id;
        }
    }
    void checkForNumber()
    {
        if (SecurePreferences.getStringValue(getApplicationContext(), "phone_number", "").equals(""))
        {
            registerPhoneSync();
        }
        else
        {
            makeLoginOfExistingPhoneSync();
        }
    }
    String parsePhoneNumber(String phoneNumber)
    {
        return phoneNumber.replaceAll("[^0-9]", "");
    }
    void registerPhoneSync()
    {
        class Task extends AsyncTask<Void, Void, Void>
        {
            private String phoneNumber="";
            private int result = 2;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                EditText editTextPhone = findViewById(R.id.editTextPhoneNumber);
                phoneNumber = editTextPhone.getText().toString();
                phoneNumber = parsePhoneNumber(phoneNumber);
                //
                SecurePreferences.setValue(getApplicationContext(), "phone_number", phoneNumber);
                setScreen(R.layout.activity_login_loading);
            }

            @Override
            protected Void doInBackground(Void... voids) {
                result = makeAuthCheckAsync(phoneNumber);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                switch (result)
                {
                    case 0:
                        registerFinalSync();
                        break;
                    case 1:
                        makeLoginOfExistingPhoneSync();
                        break;
                    case 2:
                        networkErrorSync();
                        break;
                    default:
                        break;
                }
            }
        }


        setScreen(R.layout.activity_login_enter_phone);
        Button buttonContinue = findViewById(R.id.buttonContinue);
        buttonContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Task().execute();
            }
        });


    }
    void registerFinalSync()
    {
        class Task extends AsyncTask<Void, Void, Void>
        {
            private int result = 2;
            private String phoneNumber="";
            private String secret="";
            private String name="";

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                phoneNumber = SecurePreferences.getStringValue(getApplicationContext(), "phone_number", "");
                name = ((EditText) findViewById(R.id.editTextName)).getText().toString();
                secret = ((EditText) findViewById(R.id.editTextCode)).getText().toString();
                setScreen(R.layout.activity_login_loading);
            }

            @Override
            protected Void doInBackground(Void... voids) {
                result = makeAuthNewAsync(phoneNumber, secret, name);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                switch (result)
                {

                    case 0:
                        Toast.makeText(LoginActivity.this, "Введен неправильный код потверждения", Toast.LENGTH_SHORT).show();
                        registerFinalSync();
                        break;
                    case 1:
                        goToMenuSync();
                        break;
                    case 2:
                        networkErrorSync();
                        break;
                    default:
                        break;
                }
            }

        }

        setScreen(R.layout.activity_login_enter_code);
        Button buttonLogin = findViewById(R.id.buttonLogin);
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Task().execute();
            }
        });

    }

    void makeLoginOfExistingPhoneSync()
    {

        class Task extends AsyncTask<Void, Void, Void>
        {
            private String phoneNumber="";
            private String secret="";
            private int resultAuthLogin = 0;
            private int resultAuthCheck = 2;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                phoneNumber = SecurePreferences.getStringValue(getApplicationContext(), "phone_number", "");
                secret = SecurePreferences.getStringValue(getApplicationContext(), "secret", "");

                setScreen(R.layout.activity_login_loading);
            }

            @Override
            protected Void doInBackground(Void... voids) {
                resultAuthCheck = makeAuthCheckAsync(phoneNumber);
                if (resultAuthCheck==1)
                    resultAuthLogin = makeAuthLoginAsync(phoneNumber, secret);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (resultAuthCheck==2 || resultAuthLogin==2)
                    networkErrorSync();
                else
                {
                    if (resultAuthCheck==1)
                        switch (resultAuthLogin)
                        {
                            case 0:
                                enterCodeSync();
                                break;
                            case 1:
                                goToMenuSync();
                                break;
                            default:
                                break;
                        }
                    else
                        registerFinalSync();
                }
            }
        }
        new Task().execute();
    }

    void enterCodeSync()
    {
        class Task extends AsyncTask<Void, Void, Void>
        {
            private String phoneNumber = "";
            private String secret = "";
            private int result = 2;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                phoneNumber = SecurePreferences.getStringValue(getApplicationContext(), "phone_number", "");
                secret = ((EditText) findViewById(R.id.editTextCode)).getText().toString();
                SecurePreferences.setValue(getApplicationContext(), "secret", secret);
                setScreen(R.layout.activity_login_loading);
            }

            @Override
            protected Void doInBackground(Void... voids) {
                result = makeAuthLoginAsync(phoneNumber, secret);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                switch (result)
                {
                    case 0:
                        Toast.makeText(LoginActivity.this, "Введен неправильный код потверждения", Toast.LENGTH_SHORT).show();
                        enterCodeSync();
                        break;
                    case 1:
                        goToMenuSync();
                        break;
                    case 2:
                        networkErrorSync();
                        break;
                    default:
                        break;
                }
            }
        }

        setScreen(R.layout.activity_login_enter_code);


        //setting visibility gone to name field
        EditText editTextName = findViewById(R.id.editTextName);
        editTextName.setVisibility(View.GONE);
        Button buttonLogin = findViewById(R.id.buttonLogin);
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Task().execute();
            }
        });
    }

    void goToMenuSync()
    {
        Intent intent = new Intent(this, MenuActivity.class);
        startActivity(intent);
    }

    void networkErrorSync()
    {
        setContentView(R.layout.activity_login_network_error);
        Button button3 =  findViewById(R.id.button3);
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setContentView(R.layout.activity_login_loading);
                checkForNumber();
            }
        });
    }




    int makeAuthNewAsync(String phoneNumber, String secret,String name)
    {
        try {
            Thread.sleep(300);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (phoneNumber.equals(""))
            return 0;
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url("http://app.bizzarepizza.xyz/cli/auth/new?phone="+phoneNumber+"&secret="+secret+"&name="+name).build();
        try {
            Response response =  client.newCall(request).execute();
            String result = response.body().string();
            JSONObject object = new JSONObject(result);
            int resp = object.getJSONObject("status").getInt("response");
            if (resp==200) {
                SecurePreferences.setValue(getApplicationContext(), "secret", object.getJSONObject("data").getString("secret"));
                return 1;
            }
            else
                return 0;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return 2;
    }

    int makeAuthLoginAsync(String phoneNumber, String secret)
    {
        try {
            Thread.sleep(300);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (phoneNumber.equals(""))
            return 0;
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url("http://app.bizzarepizza.xyz/cli/auth/login?phone="+phoneNumber+"&secret="+secret).build();
        try {
            Response response =  client.newCall(request).execute();
            String result = response.body().string();
            JSONObject object = new JSONObject(result);
            int resp = object.getJSONObject("status").getInt("response");
            if (resp==200) {
                SecurePreferences.setValue(getApplicationContext(), "secret", object.getJSONObject("data").getString("secret"));
                return 1;
            }
            else
                return 0;
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return 2;
    }
    int makeAuthCheckAsync(String phoneNumber)
    {
        try {
            Thread.sleep(300);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (phoneNumber.equals(""))
            return 0;
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url("http://app.bizzarepizza.xyz/cli/auth/check?phone="+phoneNumber).build();
        try {
            Response response =  client.newCall(request).execute();
            String result = response.body().string();
            JSONObject object = new JSONObject(result);
            if (object.getJSONObject("data").getBoolean("verified")) {
                return 1;
            }
            else
                return 0;
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return 2;
    }
}