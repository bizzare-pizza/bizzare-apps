package com.chopikus.bizzarepizzadrivers;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.MyViewHolder> {

    private final Context context;
    private ArrayList<OrderModel> dataSet;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textViewName;
        TextView textViewAddressFrom, textViewAddressTo;
        public MyViewHolder(View itemView) {
            super(itemView);
            this.textViewName = itemView.findViewById(R.id.textViewName);
            this.textViewAddressFrom = itemView.findViewById(R.id.textViewAddressFrom);
            this.textViewAddressTo = itemView.findViewById(R.id.textViewAddressTo);
        }
    }

    public OrderAdapter(Context context, ArrayList<OrderModel> data) {
        this.context = context; this.dataSet = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_layout, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        TextView textViewName = holder.textViewName;
        TextView textViewAddressFrom = holder.textViewAddressFrom;
        TextView textViewAddressTo = holder.textViewAddressTo;
        textViewName.setText(dataSet.get(listPosition).getName());
        textViewAddressFrom.setText(dataSet.get(listPosition).getAddressFrom());
        textViewAddressTo.setText(dataSet.get(listPosition).getAddressTo());


    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }
}