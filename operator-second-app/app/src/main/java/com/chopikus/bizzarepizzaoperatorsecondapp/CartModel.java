package com.chopikus.bizzarepizzaoperatorsecondapp;

public class CartModel {

    String name;
    int id_;
    String imageUrl;
    Integer count;

    public CartModel(String name, Integer count, int id_,  String imageUrl) {
        this.name = name;
        this.id_ = id_;
        this.imageUrl = imageUrl;
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId_() {
        return id_;
    }

    public void setId_(int id_) {
        this.id_ = id_;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}