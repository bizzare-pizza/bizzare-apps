package com.chopikus.bizzarepizza;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class UserInfoActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        final Context context = this;
        class Task extends AsyncTask<Void, Void, Void>
        {
            Boolean isResultOk = false;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                RelativeLayout relativeLayout = findViewById(R.id.relativeLayout);
                relativeLayout.setVisibility(View.GONE);
                ProgressBar bar = findViewById(R.id.progressBar3);
                bar.setVisibility(View.VISIBLE);
                TextView textViewError = findViewById(R.id.textViewError);
                Button buttonError = findViewById(R.id.buttonError);
                textViewError.setVisibility(View.GONE);
                buttonError.setVisibility(View.GONE);
            }

            @Override
            protected Void doInBackground(Void... voids) {
                String phone_number = SecurePreferences.getStringValue(getApplicationContext(), "phone_number", "");
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url("http://app.bizzarepizza.xyz/cli/info/get?phone="+phone_number+"&secret="+SecurePreferences.getStringValue(getApplicationContext(), "secret", ""))
                        .build();
                try {
                    Response response = client.newCall(request).execute();
                    JSONObject object = new JSONObject(response.body().string()).getJSONObject("data");
                    SecurePreferences.setValue(getApplicationContext(), "secret", object.getString("secret"));
                    SecurePreferences.setValue(getApplicationContext(), "name", object.getString("name"));
                    SecurePreferences.setValue(getApplicationContext(), "email", object.getString("email"));
                    isResultOk = true;
                } catch (IOException | JSONException e) {
                    e.printStackTrace();

                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

                RelativeLayout relativeLayout = findViewById(R.id.relativeLayout);

                ProgressBar bar = findViewById(R.id.progressBar3);
                bar.setVisibility(View.GONE);
                if (isResultOk) {
                    relativeLayout.setVisibility(View.VISIBLE);
                    showUserDataSync();
                }
                else
                {
                    TextView textViewError = findViewById(R.id.textViewError);
                    Button buttonError = findViewById(R.id.buttonError);
                    textViewError.setVisibility(View.VISIBLE);
                    buttonError.setVisibility(View.VISIBLE);
                    relativeLayout.setVisibility(View.GONE);
                    buttonError.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            new Task().execute();
                        }
                    });
                }
            }
        }
        new Task().execute();
    }
    void showUserDataSync()
    {
        final EditText editTextName = findViewById(R.id.editTextName);
        EditText editTextPhone = findViewById(R.id.editTextPhone);
        final EditText editTextEmail = findViewById(R.id.editTextEmail);
        EditText editTextSecret = findViewById(R.id.editTextSecret);
        editTextName.setText(SecurePreferences.getStringValue(getApplicationContext(), "name", ""));
        editTextPhone.setText(SecurePreferences.getStringValue(getApplicationContext(), "phone_number", ""));
        editTextEmail.setText(SecurePreferences.getStringValue(getApplicationContext(), "email", ""));
        editTextSecret.setText("Ваш секрет: "+SecurePreferences.getStringValue(getApplicationContext(), "secret", ""));
        Button button = findViewById(R.id.button4);
        class Task extends AsyncTask<Void, Void, Void>
        {
            ProgressDialog dialog = new ProgressDialog(UserInfoActivity.this);
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                dialog.setMessage("Обновление информации...");
                dialog.show();
            }

            @Override
            protected Void doInBackground(Void... voids) {
                OkHttpClient client = new OkHttpClient();
                String phone_number = SecurePreferences.getStringValue(getApplicationContext(), "phone_number", "");
                Request request = new Request.Builder()
                        .url("http://app.bizzarepizza.xyz/cli/info/update?phone="+phone_number+"&secret="+SecurePreferences.getStringValue(getApplicationContext(), "secret", "")+"&email="+editTextEmail.getText().toString()+"&name="+editTextName.getText().toString())
                        .build();
                try {
                    client.newCall(request).execute();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                dialog.cancel();
                finish();
            }
        }
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Task().execute();
            }
        });
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
