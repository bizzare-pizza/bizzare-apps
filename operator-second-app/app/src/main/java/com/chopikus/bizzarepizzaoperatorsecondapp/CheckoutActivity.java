package com.chopikus.bizzarepizzaoperatorsecondapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.net.URLEncoder;

import de.adorsys.android.securestoragelibrary.SecurePreferences;
import okhttp3.OkHttpClient;
import okhttp3.Request;

public class CheckoutActivity extends AppCompatActivity {

    float total_price=0;
    String products="";
    public String phone_number = "";
    public String name="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        total_price = getIntent().getFloatExtra("allPrice",0 );
        TextView textViewTotalPrice = findViewById(R.id.textViewTotalPrice);
        textViewTotalPrice.setText("Стоимость заказа: "+total_price+" грн.");
        products = getIntent().getStringExtra("products");
        String[] productsArray = products.split(";");
        final JSONArray array = new JSONArray();
        for (int i=0; i<productsArray.length/2; i++)
        {
            JSONObject object = new JSONObject();
            try {
                object.put("id", productsArray[i*2]);
                object.put("amount", productsArray[i*2+1]);
                array.put(object);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        PlaceAutocompleteFragment places= (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                .setCountry("UA")
                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_ADDRESS)
                .build();
        places.setFilter(typeFilter);
        GeoLocation location = GeoLocation.fromDegrees(48.9226277, 24.6760977);
        GeoLocation[] coordinates = location.boundingCoordinates(0.1, 6371.01);
        places.setBoundsBias(new LatLngBounds(new LatLng(coordinates[0].getLatitudeInDegrees(), coordinates[0].getLongitudeInDegrees()), new LatLng(coordinates[1].getLatitudeInDegrees(), coordinates[1].getLongitudeInDegrees())));
        places.setHint("Введите адрес доставки..");
        final String[] chosen_address = {""};

        final Button checkout = (Button) findViewById(R.id.checkout);
        checkout.setEnabled(false);
        places.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                if (place.isDataValid()) {
                    chosen_address[0] = String.valueOf(place.getAddress());
                    checkout.setEnabled(true);
                }
                else
                    checkout.setEnabled(false);
            }

            @Override
            public void onError(Status status) {

            }
        });



        final Context context = this;
        class MyTask extends AsyncTask<Void, Void, Void> {
            ProgressDialog dialog;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                dialog = ProgressDialog.show(context, "Загрузка...",
                        "Оформление заказа. Пожалуйста, подождите", true);
                EditText editText2 = (EditText) findViewById(R.id.editText2);
                EditText editText3 = (EditText) findViewById(R.id.editText3);
                phone_number = editText2.getText().toString();
                name = editText3.getText().toString();
            }

            @Override
            protected Void doInBackground(Void... params) {
                try {
                    JSONObject object = new JSONObject();
                    object.put("dishes", array);
                    object.put("phone", phone_number);
                    object.put("address", chosen_address[0]);
                    JSONObject object1 = new JSONObject();
                    object1.put("phone", phone_number);
                    object1.put("name", name);
                    String str1 = "http://app.bizzarepizza.xyz/opr/client/register?login=test_operator&token=3&data="+object1.toString();
                    String str2 = "http://app.bizzarepizza.xyz/opr/client/order?login=test_operator&token=3&data="+object.toString();
                    OkHttpClient client = new OkHttpClient();
                    Request request = new Request.Builder()
                            .url(str1)
                            .build();
                    Request request1 = new Request.Builder()
                            .url(str2)
                            .build();
                    client.newCall(request).execute();
                    client.newCall(request1).execute();

                } catch (Exception e)
                {
                    Log.e("EXCEPTION", e.getMessage());
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                super.onPostExecute(result);
                dialog.cancel();
                SharedPreferences preferences = getSharedPreferences("other_info", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor= preferences.edit();
                SharedPreferences preferences1 = getSharedPreferences("pizzas", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor1 = preferences1.edit();
                editor1.clear();
                editor.apply();
                editor1.apply();
                finish();
            }

        }

        checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new MyTask().execute();
            }
        });


    }
}
