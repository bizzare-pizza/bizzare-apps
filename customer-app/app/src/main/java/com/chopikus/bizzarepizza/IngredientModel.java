package com.chopikus.bizzarepizza;

public class IngredientModel {

    String name;
    String amount="";
    String measurementUnit="";

    public IngredientModel(String name, String amount, String measurementUnit) {
        this.name = name;
        this.amount = amount;
        this.measurementUnit = measurementUnit;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getMeasurementUnit() {
        return measurementUnit;
    }

    public void setMeasurementUnit(String measurementUnit) {
        this.measurementUnit = measurementUnit;
    }
}