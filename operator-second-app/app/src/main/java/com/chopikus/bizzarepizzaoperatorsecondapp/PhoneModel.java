package com.chopikus.bizzarepizzaoperatorsecondapp;

public class PhoneModel {

    String phoneNumber;
    String token="";
    int id=0;
    public PhoneModel(String phoneNumber, String token, int id) {
        this.phoneNumber = phoneNumber;
        this.token = token;
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}