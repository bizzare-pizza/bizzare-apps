package com.chopikus.bizzarepizza;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class PizzaActivity extends AppCompatActivity {
    String pizza_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pizza);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        pizza_id = getIntent().getStringExtra("pizza_id");
        final String minutes = getIntent().getStringExtra("minutes");
        final String pizza_name = getIntent().getStringExtra("pizza_name");
        final SharedPreferences sharedPreferences = getSharedPreferences("pizzas", Context.MODE_PRIVATE);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sharedPreferences.getInt(pizza_id, 0)==8)
                {
                    Toast.makeText(PizzaActivity.this, "Ошибка: слишком большой заказ!", Toast.LENGTH_SHORT).show();
                    return;
                }
                final SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putInt(pizza_id, sharedPreferences.getInt(pizza_id, 0) + 1);
                editor.apply();
                Snackbar.make(view, "Продукт был добавлен в корзину", Snackbar.LENGTH_LONG)
                        .setAction("Отмена", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                if (sharedPreferences.getInt(pizza_id, 0) == 1) {
                                    editor.remove(pizza_id);
                                } else
                                    editor.putInt(pizza_id, sharedPreferences.getInt(pizza_id, 0) - 1);
                                editor.apply();
                            }
                        }).show();

            }
        });

        class MyTask extends AsyncTask<Void, Void, Void> {
            Toolbar toolbar;
            TextView longTextView;
            private String pizza_desc = "";
            ProgressDialog dialog;
            private String phone_number = "";
            private ArrayList<IngredientModel> ingredients = new ArrayList<>();

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                toolbar = (Toolbar) findViewById(R.id.toolbar);
                toolbar.setTitle("");
                setSupportActionBar(toolbar);
                longTextView = (TextView) findViewById(R.id.longTextView);
                dialog = ProgressDialog.show(PizzaActivity.this, "", "Загрузка...");
                if (getSupportActionBar() != null) {
                    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                    getSupportActionBar().setDisplayShowHomeEnabled(true);
                }
                phone_number = SecurePreferences.getStringValue(getApplicationContext(), "phone_number", "");

            }

            @Override
            protected Void doInBackground(Void... params) {
                try {
                    OkHttpClient client = new OkHttpClient();

                    String url = "http://app.bizzarepizza.xyz/cli/dish/info?phone=" + phone_number + "&secret=" + SecurePreferences.getStringValue(getApplicationContext(), "secret", "") + "&data=%7B%22id%22:" + pizza_id + "%7D";
                    Request request = new Request.Builder().url(url).build();
                    Response response = client.newCall(request).execute();
                    String out = response.body().string();
                    JSONObject object = new JSONObject(out);
                    pizza_desc = object.getJSONObject("data").getString("description");
                    JSONArray array = object.getJSONObject("data").getJSONArray("ingredients");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object1 = array.getJSONObject(i);
                        ingredients.add(new IngredientModel(object1.getString("name"), object1.getString("amount"), object1.getString("measurement_unit")));
                    }
                } catch (Exception e) {
                    Log.d("URLURLRESULT", e.getMessage());
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                super.onPostExecute(result);
                toolbar.setTitle(pizza_name);

                longTextView.setText("Время приготовления: " + minutes + " минут \n" + pizza_desc);
                RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerViewProducts);
                IngredientAdapter adapter = new IngredientAdapter(ingredients);
                recyclerView.setAdapter(adapter);
                recyclerView.setLayoutManager(new LinearLayoutManager(PizzaActivity.this));
                recyclerView.setItemAnimator(new DefaultItemAnimator());

                dialog.cancel();
            }
        }
        MyTask task = new MyTask();
        task.execute();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
