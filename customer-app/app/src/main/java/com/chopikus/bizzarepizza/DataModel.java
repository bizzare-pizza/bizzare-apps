package com.chopikus.bizzarepizza;

public class DataModel {

    String name;
    int id_;
    String imageUrl;
    Double price;
    String amountUnit="";
    String minutes;
    public DataModel(String name, Double price,  int id_, String imageUrl, String amountUnit, String minutes) {
        this.name = name;
        this.price = price;
        this.id_ = id_;
        this.imageUrl=imageUrl;
        this.amountUnit = amountUnit;
        this.minutes = minutes;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId_() {
        return id_;
    }

    public void setId_(int id_) {
        this.id_ = id_;
    }

    public void setImage(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getAmountUnit() {
        return amountUnit;
    }

    public void setAmountUnit(String amountUnit) {
        this.amountUnit = amountUnit;
    }

    public String getName() {
        return name;
    }

    public Double getPrice() {
        return price;
    }

    public String getImageUrl() {
        return imageUrl;
    }

}