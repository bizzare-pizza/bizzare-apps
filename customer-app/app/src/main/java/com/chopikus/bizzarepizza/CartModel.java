package com.chopikus.bizzarepizza;

import android.graphics.Bitmap;

public class CartModel {

    String name;
    int id_;
    String imageLink;
    Integer count;
    public CartModel(String name, Integer count, int id_, String imageLink) {
        this.name = name;
        this.count = count;
        this.id_ = id_;
        this.imageLink=imageLink;
    }

    public String getName() {
        return name;
    }

    public Integer getCount() {
        return count;
    }

    public String getImageLink() {
        return imageLink;
    }

    public int getId() {
        return id_;
    }
}