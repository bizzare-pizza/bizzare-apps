package com.chopikus.bizzarepizzaoperatorsecondapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import de.adorsys.android.securestoragelibrary.SecurePreferences;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MenuActivity extends AppCompatActivity {

    private static RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private static RecyclerView recyclerView;
    private static ArrayList<DataModel> data;
    static View.OnClickListener myOnClickListener;
    private static ArrayList<Integer> removedItems;
    private String phone_number = "";
    private Boolean found = false;
    private Spinner spinner;

    public static Bitmap drawableToBitmap(Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if (bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if (drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }


    private class MyOnClickListener implements View.OnClickListener {

        private MyOnClickListener(Context context) {
        }

        @Override
        public void onClick(View v) {
            TextView textViewId = v.findViewById(R.id.textViewId);
            //Toast.makeText(context, textView.getText(), Toast.LENGTH_SHORT).show();
            //context.startActivity(new Intent(context, PizzaActivity.class));
            final String pizza_id = textViewId.getText().toString();
            final SharedPreferences sharedPreferences = getSharedPreferences("pizzas", Context.MODE_PRIVATE);
            final SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putInt(pizza_id, sharedPreferences.getInt(pizza_id, 0) + 1);
            editor.apply();
            Snackbar.make(v, "Продукт был добавлен в корзину", Snackbar.LENGTH_LONG)
                    .setAction("Отмена", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            if (sharedPreferences.getInt(pizza_id, 0) == 1) {
                                editor.remove(pizza_id);
                            } else
                                editor.putInt(pizza_id, sharedPreferences.getInt(pizza_id, 0) - 1);
                            editor.apply();
                        }
                    }).show();


        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_pizza, menu);
        MenuItem item = menu.findItem(R.id.spinner);
        spinner = (Spinner) item.getActionView();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return true;
    }


    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }

    private String saveToInternalStorage(Bitmap bitmapImage, String name) {
        ContextWrapper cw = new ContextWrapper(getApplicationContext());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath = new File(directory, name);

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return directory.getAbsolutePath();
    }

    public void loadList() {

        myOnClickListener = new MyOnClickListener(this);
        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        data = new ArrayList<DataModel>();

        final Context context = this;
        class MyTask extends AsyncTask<Void, Void, Void> {
            ProgressDialog dialog;
            private Response response;
            private String result;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                dialog = ProgressDialog.show(context, "Загрузка...",
                        "Загрузка меню. Пожалуйста, подождите", true);

            }

            @Override
            protected Void doInBackground(Void... params) {
                OkHttpClient okHttpClient = new OkHttpClient();
                Request request = new Request.Builder()
                        .url("http://app.bizzarepizza.xyz/opr/dishcategory/list?login=test_operator&token=3")
                        .build();
                try {
                    response = okHttpClient.newCall(request).execute();
                    result = response.body().string();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    SharedPreferences preferences = getSharedPreferences("pizzas", Context.MODE_PRIVATE);
                    SharedPreferences other_info = getSharedPreferences("other_info", Context.MODE_PRIVATE);
                    SharedPreferences.Editor info_edit = other_info.edit();
                    SharedPreferences.Editor editor = preferences.edit();
                    String linkpart = "";
                    if (!SecurePreferences.getStringValue("category", "").equals("")) {
                        linkpart += "&category=" + SecurePreferences.getStringValue("category", "");
                    }
                    request = new Request.Builder().url("http://app.bizzarepizza.xyz/opr/dish/list?login=test_operator&token=3"+linkpart).build();
                    response = okHttpClient.newCall(request).execute();
                    String out = response.body().string();
                    JSONObject object = new JSONObject(out);
                    JSONArray array = object.getJSONArray("data");
                    for (int i = 0; i < array.length(); i++) {
                        String name = array.getJSONObject(i).getString("name");
                        Double price = array.getJSONObject(i).getDouble("price");
                        int id = array.getJSONObject(i).getInt("id");
                        String bitmapUrl = array.getJSONObject(i).getString("photo");
                        info_edit.putString("" + id, name);
                        info_edit.putFloat("price" + id, price.floatValue());
                        info_edit.putString("bitmapUrl" + id, bitmapUrl);
                        String amountUnit = array.getJSONObject(i).getString("amount") + " " + array.getJSONObject(i).getString("measurement_unit");
                        String minutes = array.getJSONObject(i).getString("cooking_time");
                        data.add(new DataModel(name, price, id, bitmapUrl, amountUnit, minutes));
                    }
                    editor.apply();
                    info_edit.apply();
                }
                catch (Exception ignored) {}
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                dialog.cancel();
                JSONArray array = null;
                final ArrayList<String> list = new ArrayList<>();
                list.add("все");
                try {
                    JSONObject object = new JSONObject(result);
                    array = object.getJSONArray("data");


                    for (int i = 0; i < array.length(); i++) {
                        try {
                            list.add(array.getString(i));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(MenuActivity.this, android.R.layout.simple_spinner_item, list);
                adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(adapter2);
                spinner.setSelection(SecurePreferences.getIntValue("categoryIndex", 0));
                final int[] currentItem = {SecurePreferences.getIntValue("categoryIndex", 0)};
                spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        if (currentItem[0] != i) {
                            if (i == 0)
                                SecurePreferences.setValue("category", "");
                            else
                                SecurePreferences.setValue("category", list.get(i));
                            SecurePreferences.setValue("categoryIndex", i);
                            loadList();
                        }
                        currentItem[0] = i;
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
                SwipeRefreshLayout layout = findViewById(R.id.swipeRefreshLayout);
                layout.setRefreshing(false);
                layout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        loadList();
                    }
                });
                if (data.size() == 0) {
                    recyclerView.setVisibility(View.GONE);
                    LinearLayout linearLayout = (LinearLayout) findViewById(R.id.linearLayout);
                    linearLayout.setVisibility(View.VISIBLE);
                    LinearLayout linearLayout2 = (LinearLayout) findViewById(R.id.linearLayout2);
                    linearLayout2.setVisibility(View.GONE);

                    Button retryButton = (Button) findViewById(R.id.retryButton);
                    retryButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            recyclerView.setVisibility(View.VISIBLE);
                            loadList();
                        }
                    });
                } else {
                    adapter = new CustomAdapter(data);
                    recyclerView.setAdapter(adapter);
                    LinearLayout linearLayout = (LinearLayout) findViewById(R.id.linearLayout);
                    linearLayout.setVisibility(View.GONE);
                    LinearLayout linearLayout2 = (LinearLayout) findViewById(R.id.linearLayout2);
                    linearLayout2.setVisibility(View.VISIBLE);
                }
            }

        }
        new MyTask().execute();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        loadList();

    }

    public void openCart(View view) {
        Intent intent = new Intent(this, CartActivity.class);
        startActivity(intent);
    }

}
