package com.chopikus.bizzarepizza;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.MyViewHolder> {

    private final Context context;
    private ArrayList<OrderModel> dataSet;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textViewName;
        TextView textViewAddress;
        ImageView imageView;
        public MyViewHolder(View itemView) {
            super(itemView);
            this.textViewName = itemView.findViewById(R.id.textViewName);
            this.textViewAddress = itemView.findViewById(R.id.textViewAddress);
            this.imageView = itemView.findViewById(R.id.imageView);
        }
    }

    public OrderAdapter(Context context, ArrayList<OrderModel> data) {
        this.context = context; this.dataSet = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_layout, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        TextView textViewName = holder.textViewName;
        TextView textViewAddress = holder.textViewAddress;
        ImageView imageView = holder.imageView;
        textViewName.setText(dataSet.get(listPosition).getName());
        textViewAddress.setText(dataSet.get(listPosition).getAddress());
        if (dataSet.get(listPosition).getStatus().equals("uncertain"))
        {
            imageView.setImageResource(R.drawable.uncertain);
        }
        if (dataSet.get(listPosition).getStatus().equals("processing"))
        {
            imageView.setImageResource(R.drawable.processing);
        }
        if (dataSet.get(listPosition).getStatus().equals("delivery"))
        {
            imageView.setImageResource(R.drawable.delivery);
        }


    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }
}