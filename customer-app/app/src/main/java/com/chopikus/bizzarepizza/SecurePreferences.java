package com.chopikus.bizzarepizza;

/*
    This class is provided to save data, which is not properly working on SecurePreferences library
 */

import android.content.Context;
import android.content.SharedPreferences;

class SecurePreferences {
    public static void setValue(Context context, String key, String value)
    {
        SharedPreferences pref = context.getSharedPreferences("eSCBCbDeGX", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static void setValue(Context context, String key, int value)
    {
        SharedPreferences pref = context.getSharedPreferences("eSCBCbDeGX", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public static String getStringValue(Context context, String key, String defaultValue)
    {
        SharedPreferences pref = context.getSharedPreferences("eSCBCbDeGX", Context.MODE_PRIVATE);
        return pref.getString(key, defaultValue);
    }
    public static int getIntValue(Context context, String key, int defaultValue)
    {
        SharedPreferences pref = context.getSharedPreferences("eSCBCbDeGX", Context.MODE_PRIVATE);
        return pref.getInt(key, defaultValue);
    }

}
