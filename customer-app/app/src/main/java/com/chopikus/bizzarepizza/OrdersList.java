package com.chopikus.bizzarepizza;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class OrdersList extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders_list);

        if (getSupportActionBar()!=null)
        {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        Task task = new Task();
        task.showDialog = true;
        task.execute();
    }
    class Task extends AsyncTask<Void, Void, Void>
    {

        private String phone_number;
        ArrayList<OrderModel> arrayList;
        Boolean showDialog = true;
        private Boolean loaded = true;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loaded = true;
            TextView textView4 = findViewById(R.id.textView4);
            textView4.setVisibility(View.GONE);

            FrameLayout frameLayout = findViewById(R.id.frameLayout);
            RelativeLayout relativeLayout = findViewById(R.id.relativeLayout);
            if (showDialog)
            {
                relativeLayout.setVisibility(View.GONE);
                frameLayout.setVisibility(View.VISIBLE);
            }
            phone_number = SecurePreferences.getStringValue(getApplicationContext(), "phone_number", "");
            arrayList = new ArrayList<>();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url("http://app.bizzarepizza.xyz/cli/maybeorder/list?phone="+phone_number+"&secret="+SecurePreferences.getStringValue(getApplicationContext(), "secret", ""))
                    .build();
            Request request2 = new Request.Builder()
                    .url("http://app.bizzarepizza.xyz/cli/order/list?phone="+phone_number+"&secret="+SecurePreferences.getStringValue(getApplicationContext(), "secret", ""))
                    .build();
            Request request3 = new Request.Builder()
                    .url("http://app.bizzarepizza.xyz/cli/delivery/list?phone="+phone_number+"&secret="+SecurePreferences.getStringValue(getApplicationContext(), "secret", ""))
                    .build();

            try {
                Response response = client.newCall(request).execute();
                String s = response.body().string();
                JSONObject object1 = new JSONObject(s);
                JSONArray array = object1.getJSONArray("data");
                for (int i=0; i<array.length(); i++)
                {
                    JSONObject object = array.getJSONObject(i);
                    arrayList.add(new OrderModel("Заказ №"+object.getString("number"), object.getString("address"), "uncertain"));
                }
            } catch (IOException e) {
                loaded = false;
                e.printStackTrace();
            } catch (JSONException e) {
                loaded = false;
                e.printStackTrace();
            }
            try {
                Response response = client.newCall(request2).execute();
                String s = response.body().string();
                JSONObject object1 = new JSONObject(s);
                JSONArray array = object1.getJSONArray("data");
                for (int i=0; i<array.length(); i++)
                {
                    JSONObject object = array.getJSONObject(i);
                    arrayList.add(new OrderModel("Заказ №"+object.getString("number"), object.getString("address"), "processing"));
                }
            } catch (IOException e) {
                loaded = false;
                e.printStackTrace();
            } catch (JSONException e) {
                loaded = false;
                e.printStackTrace();
            }
            try {
                Response response = client.newCall(request3).execute();
                String s = response.body().string();
                JSONObject object1 = new JSONObject(s);
                JSONArray array = object1.getJSONArray("data");
                for (int i=0; i<array.length(); i++)
                {
                    JSONObject object = array.getJSONObject(i);
                    arrayList.add(new OrderModel("Заказ №"+object.getString("number"), object.getString("address"), "delivery"));
                }
            } catch (IOException e) {
                loaded = false;
                e.printStackTrace();
            } catch (JSONException e) {
                loaded = false;
                e.printStackTrace();
            }
            if (!loaded)
                arrayList.clear();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            FrameLayout frameLayout = findViewById(R.id.frameLayout);
            RelativeLayout relativeLayout = findViewById(R.id.relativeLayout);
            if (showDialog)
            {
                relativeLayout.setVisibility(View.VISIBLE);
                frameLayout.setVisibility(View.GONE);
            }
            final TextView textView4 = findViewById(R.id.textView4);

            if (arrayList.size()!=0)
                textView4.setVisibility(View.GONE);
            else {
                textView4.setVisibility(View.VISIBLE);
                if (!loaded)
                {
                    textView4.setText("Произошла ошибка при загрузке с сервера. Попробуйте еще раз, потянув сверху.");
                }
                else
                    textView4.setText("Список заказов пуст!");
            }

            SwipeRefreshLayout mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refreshLayout);
            mSwipeRefreshLayout.setRefreshing(false);
            mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    Task task = new Task();
                    task.showDialog = false;
                    task.execute();
                }
            });
            RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
            recyclerView.setLayoutManager(new LinearLayoutManager(OrdersList.this));
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            OrderAdapter adapter = new OrderAdapter(OrdersList.this, arrayList);
            recyclerView.setAdapter(adapter);

        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId()==android.R.id.home)
        {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
