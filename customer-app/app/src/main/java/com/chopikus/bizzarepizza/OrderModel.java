package com.chopikus.bizzarepizza;

public class OrderModel {

    String name="", address="",status="";

    public OrderModel(String name, String address, String status) {
        this.name = name;
        this.address = address;
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}