# Bizzare Pizza Android Apps

This repository contains Android apps for Bizzare Pizza, such as:
* App for customers [customer-app](/customer-app)
* First app for operators [operator-app](/operator-app)
* Second app for operators [operator-second-app](/operator-second-app)
* App for managers [manager-app](/managerapp)
* App for drivers [BizzarePizzaDrivers](/BizzarePizzaDrivers)
# Content
* [Technologies and libraries used](#technologies_used)  
* [Apps structure](#apps_structure)  
  * [Customer App](#customer_app_structure)  
  * [Operator App](#operator_app_structure)
  * [Manager App](#manager_app_structure)
  * [Cook App](#cook_app_structure)
* [What had been done?](#what_had_been_done)  
  * [Customer App](#customer_app_done) 
  * [First operator app](#operator_app_done)
  * [Second operator app](#second_operator_app_done)
  * [Manager App](#manager_app_done)
  
<a name="technologies_used"><h2>Technologies and libraries used</h2></a>
* Language : Java
* Android SDK
* OK HTTP client
* Android Printing API 

<a name="apps_structure"><h2>Apps structure</h2></a>

<a name="customer_app_structure"><h3>Customer App</h3></a>

|File|Purpose|
|:-:|:-:|
|CartActivity.java|Activity of changing cart before checkout|
|CartAdapter.java|RecyclerView cart list adapter|
|CartModel.java|Class that decribes cart's item|
|CheckoutActivity.java|Activity where user enters all needed info for processing order|
|CustomAdapter.java|RecyclerView menu adapter|
|DataModel.java|Class that describes menu product|
|ImageSaver.java|Class for saving images in memory|
|MenuActivity.java|Menu Activity|
|PizzaActivity.java|Info of specific product|

<a name="operator_app_structure"><h3>Operator App</h3></a>

#### Classes
|File|Purpose|
|:-:|:-:|
|MainActivity.java|Main Activity|
|OperatorAdapter.java|RecyclerView operator list adapter|
|OrderModel.java|Class that describes order|

<a name="manager_app_structure"><h3>Manager App</h3></a>


#### Classes
|File|Purpose|
|:-:|:-:|
|MainActivity.java|Main Activity|
|AddCafe.java|Activity for adding cafe|
|AddSupply.java|Activity for adding supply|
|CafeAdapter.java|Recyclerview cafe adapter|
|CafeModel.java|Class describing cafe|
|EditCafe.java|Activity for editing cafe|
|FoodStuffAdapter.java|Foodstuff recyclerView adapter|
|FoodStuffModel.java|Class describing foodstuff|
|ProductsListActivity.java|Activity for viewing products list|
|RecyclerItemClickListener.java|Class for listening item clicks|
|SupplierAdapter.java|RecyclerView supplier adapter|
|SupplierModel.java|Class, describes supplier|
|SupplyAdapter.java|RecyclerView supply dapter|
|SupplyAddProducts.java|Activity for adding supply|
|SupplyAddProductsAdapter.java|RecyclerView adapter for adding supplies|
|SupplyModel.java|Class, describes supply|
|SupplyProductsAdapter.java|RecyclerView supplyproducts adapter|
|SupplyProductsModel.java|Class, describles supply's product|
|AddDishActivity.java|Activity for adding dish|



<a name="what_had_been_done"><h2>What had been done?</h2></a>

<a name="customer_app_done"><h3>Customer app</h3></a>

| Task | Completed? |
|------|:------------:|
|List of products|:white_check_mark:|
|Description of specific product|:white_check_mark:|
|Working with cart|:white_check_mark:|
|Making Order|:white_check_mark:|

<a name="operator_app_done"><h3>Operator app</h3></a>

| Task | Completed? |
|------|:------------:|
|List of orders|:white_check_mark:|
|Change order status|:white_check_mark:|
|Printing|:white_check_mark:|

<a name="manager_app_done"><h3>Manager App</h3></a>

| Task | Completed? |
|------|:------------:|
|Changing menu|:white_check_mark:|
|Changing list of ingridients of specific product|:white_check_mark:|
|Working with supplies|:white_check_mark:|
|Changing cafes list|:white_check_mark:|

