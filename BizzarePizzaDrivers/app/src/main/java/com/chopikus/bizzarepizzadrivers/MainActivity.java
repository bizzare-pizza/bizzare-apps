package com.chopikus.bizzarepizzadrivers;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import de.adorsys.android.securestoragelibrary.SecurePreferences;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private static String phone_number;
    public Button button;
    public int tabPosition = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        tryAuth();
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);

        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

    }

    public void load() {
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mSectionsPagerAdapter);
    }

    /*
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);*/
    public void sendCodeDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Введите код");
        View view = getLayoutInflater().inflate(R.layout.phone_code_dialog, null);
        builder.setView(view);
        EditText editTextName = (EditText) view.findViewById(R.id.editTextName);
        editTextName.setVisibility(View.GONE);
        final EditText editTextCode = (EditText) view.findViewById(R.id.editText8);

        class MyTask2 extends AsyncTask<Void, Void, Void> {
            private ProgressDialog dialog = new ProgressDialog(MainActivity.this);
            private String code = "";
            private Boolean result = false;
            private String newCode = "";

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                code = editTextCode.getText().toString();

                dialog.setMessage("Загрузка...");
                dialog.show();
            }

            @Override
            protected Void doInBackground(Void... voids) {
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder().url("http://app.bizzarepizza.xyz/drv/auth/login?phone=" + phone_number + "&secret=" + code).build();
                try {
                    Response response = client.newCall(request).execute();
                    String responseString = "";
                    if (response.body() != null) {
                        responseString = response.body().string();
                    }
                    JSONObject object = new JSONObject(responseString);
                    if (object.getJSONObject("status").getInt("response") == 200) {
                        newCode = object.getJSONObject("data").getString("secret");
                        result = true;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                dialog.cancel();
                if (result) {
                    if (!newCode.equals("")) {
                        SecurePreferences.setValue("secret", newCode);
                        tryAuth();
                    }

                }
            }

        }

        builder.setNegativeButton("Закрыть", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        builder.setPositiveButton("Готово", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                new MyTask2().execute();
            }
        });
        builder.show();
    }

    public void login() {

        class Task extends AsyncTask<Void, Void, Boolean> {
            ProgressDialog dialog = new ProgressDialog(MainActivity.this);

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                dialog.setMessage("загрузка...");
                dialog.show();
            }

            @Override
            protected Boolean doInBackground(Void... voids) {
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder().url("http://app.bizzarepizza.xyz/drv/auth/login?phone=" + phone_number + "&secret=" + SecurePreferences.getStringValue("secret", "")).build();
                try {
                    Response response = client.newCall(request).execute();
                    String result = response.body().string();
                    JSONObject object = new JSONObject(result);
                    if (object.getJSONObject("status").getInt("response") == 200) {
                        SecurePreferences.setValue("secret", object.getJSONObject("data").getString("secret"));
                        return true;
                    } else
                        return false;
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return false;
            }

            @Override
            protected void onPostExecute(Boolean b) {
                super.onPostExecute(b);
                dialog.cancel();
                if (!b)
                    sendCodeDialog();
                else
                    load();
            }
        }

        new Task().execute();
    }

    public void onButtonClick(View view) {
        tryAuth();
    }

    public void enterPhoneDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Введите код");
        final View view = getLayoutInflater().inflate(R.layout.enter_phone_dialog, null);
        builder.setView(view);
        builder.setPositiveButton("Продолжить", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                EditText editText = view.findViewById(R.id.editTextPhone);
                SecurePreferences.setValue("phoneNumber", editText.getText().toString().replaceAll("[^0-9]", ""));
                tryAuth();
            }
        });
        builder.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.show();
    }

    public void tryAuth() {
        try {


            final ProgressDialog dialog = new ProgressDialog(this);
            dialog.setMessage("Загрузка...");
            class Task extends AsyncTask<Void, Void, Void> {
                private Response response;
                private String responseString;
                private Boolean check = true;

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();

                    if (SecurePreferences.getStringValue("phoneNumber", "").equals("")) {
                        enterPhoneDialog();
                        check = false;
                        return;
                    } else {
                        phone_number = SecurePreferences.getStringValue("phoneNumber", "");
                    }
                    dialog.show();
                }

                @Override
                protected Void doInBackground(Void... voids) {
                    if (!check)
                        return null;
                    final OkHttpClient client = new OkHttpClient();
                    Request request = new Request.Builder()
                            .url("http://app.bizzarepizza.xyz/drv/auth/check?phone=" + phone_number).build();
                    try {
                        response = client.newCall(request).execute();
                        if (response.body() != null)
                            responseString = response.body().string();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);


                    if (!check)
                        return;
                    dialog.dismiss();
                    try {
                        final JSONObject object = new JSONObject(responseString);
                        if (!object.getJSONObject("data").getBoolean("verified")) {
                            Toast.makeText(MainActivity.this, "Ваш аккаунт не был потвержден. Ожидайте звонка от нашего администратора.", Toast.LENGTH_SHORT).show();
                        } else
                            login();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            new Task().execute();


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        private RecyclerView.OnItemTouchListener onItemTouchListener;

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public void setUserVisibleHint(boolean isVisibleToUser) {

            super.setUserVisibleHint(
                    isVisibleToUser);

            // Refresh tab data:

            if (getFragmentManager() != null) {

                getFragmentManager()
                        .beginTransaction()
                        .detach(this)
                        .attach(this)
                        .commit();
            }
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            // getArguments().getInt(ARG_SECTION_NUMBER)
            final RecyclerView recyclerView = rootView.findViewById(R.id.recyclerView);
            final ProgressBar bar = rootView.findViewById(R.id.progressBar);
            final Button button = rootView.findViewById(R.id.button);

            class ExecuteTask extends AsyncTask<String, Void, Void> {
                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                }

                @Override
                protected Void doInBackground(String... strings) {
                    OkHttpClient client = new OkHttpClient();
                    Request request = new Request.Builder()
                            .url(strings[0])
                            .build();
                    try {
                        client.newCall(request).execute();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    setUserVisibleHint(true);
                }
            }
            class Task extends AsyncTask<String, Void, Void> {
                private double lng = 0, lat = 0;
                ArrayList<OrderModel> arrayList = new ArrayList<>();
                JSONArray arrIntent = new JSONArray();

                public Double[] getLatLongFromGivenAddress(String youraddress) {
                    String uri = "https://maps.googleapis.com/maps/api/geocode/json?address=" +
                            youraddress + "&key=AIzaSyA-uUv19GD3J6fGoxL-XzgYxUU1y-zhWjo";
                    OkHttpClient client = new OkHttpClient();
                    Request request = new Request.Builder().url(uri).build();
                    Response response = null;
                    try {
                        response = client.newCall(request).execute();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    JSONObject jsonObject;
                    try {
                        jsonObject = new JSONObject(response.body().string());
                        lng = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                                .getJSONObject("geometry").getJSONObject("location")
                                .getDouble("lng");

                        lat = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                                .getJSONObject("geometry").getJSONObject("location")
                                .getDouble("lat");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Double[] arr = new Double[2];
                    arr[0] = lat;
                    arr[1] = lng;
                    return arr;

                }

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    bar.setVisibility(View.VISIBLE);
                    phone_number = SecurePreferences.getStringValue("phoneNumber", "");

                }

                @Override
                protected Void doInBackground(String... strings) {
                    OkHttpClient client = new OkHttpClient();
                    Request request = new Request.Builder()
                            .url("http://app.bizzarepizza.xyz/drv/" + strings[0] + "/list?phone=" + phone_number + "&secret=" + SecurePreferences.getStringValue("secret", "")).build();
                    try {
                        String response = client.newCall(request).execute().body().string();
                        JSONObject object = new JSONObject(response);
                        JSONArray array = object.getJSONArray("data");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject object1 = array.getJSONObject(i);
                            String addressFrom = object1.getString("cafe_address");
                            String addressTo = object1.getString("address");
                            JSONObject object2 = new JSONObject();
                            Double[] arr = getLatLongFromGivenAddress(addressFrom);
                            object2.put("lat", arr[0]);
                            object2.put("lng", arr[1]);
                            arrIntent.put(object2);
                            object2 = new JSONObject();
                            arr = getLatLongFromGivenAddress(addressTo);
                            object2.put("lat", arr[0]);
                            object2.put("lng", arr[1]);
                            arrIntent.put(object2);
                            arrayList.add(new OrderModel("Заказ №" + object1.getString("number"), addressFrom, addressTo, object1.getString("id"), object1.getString("number")));
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    bar.setVisibility(View.GONE);
                    recyclerView.setHasFixedSize(true);
                    OrderAdapter adapter = new OrderAdapter(getActivity(), arrayList);
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
                    linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                    recyclerView.setLayoutManager(linearLayoutManager);
                    recyclerView.setAdapter(adapter);
                    LinearLayout layout = getActivity().findViewById(R.id.layout);
                    if (arrayList.size() != 0) {
                        layout.setVisibility(View.GONE);
                    }
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(getActivity(), MapsActivity.class);
                            intent.putExtra("allPointsJSON", arrIntent.toString());
                            startActivity(intent);
                        }
                    });
                    onItemTouchListener = new RecyclerItemClickListener(getActivity(), recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, final int position) {
                            if (getArguments().getInt(ARG_SECTION_NUMBER) == 1) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                builder.setTitle("Взять заказ?");
                                builder.setMessage("Взять заказ №" + arrayList.get(position).getId() + "?");
                                builder.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                                builder.setPositiveButton("Взять", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        new ExecuteTask().execute("http://app.bizzarepizza.xyz/drv/delivery/claim?phone=" + phone_number + "&secret=" + SecurePreferences.getStringValue("secret", "") + "&data={\"id\":" + arrayList.get(position).getId() + "}");
                                    }
                                });
                                builder.show();
                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                builder.setTitle("Изменить статус заказа №" + arrayList.get(position).getId());
                                builder.setMessage("Взять заказ №" + arrayList.get(position).getId());
                                builder.setNeutralButton("Закрыть", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                                builder.setPositiveButton("Заказ был доставлен", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        new ExecuteTask().execute("http://app.bizzarepizza.xyz/drv/claim/confirm?phone=" + phone_number + "&secret=" + SecurePreferences.getStringValue("secret", "") + "&data={\"id\":" + arrayList.get(position).getId() + "}");
                                    }
                                });
                                builder.setNegativeButton("Заказ не удалось доставить", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        new ExecuteTask().execute("http://app.bizzarepizza.xyz/drv/claim/decline?phone=" + phone_number + "&secret=" + SecurePreferences.getStringValue("secret", "") + "&data={\"id\":" + arrayList.get(position).getId() + "}");
                                    }
                                });
                                builder.show();
                            }
                        }

                        @Override
                        public void onLongItemClick(View view, int position) {

                        }
                    });
                    recyclerView.addOnItemTouchListener(onItemTouchListener);
                }
            }
            if (getArguments().getInt(ARG_SECTION_NUMBER) == 1) {
                button.setVisibility(View.GONE);
                new Task().execute("delivery");
            } else {
                button.setVisibility(View.VISIBLE);
                new Task().execute("claim");
            }
            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 2 total pages.
            return 2;
        }
    }

}
